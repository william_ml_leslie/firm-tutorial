from firmetude.driver import compile
import sys

filename = sys.argv[1]

with open(filename, 'r') as inp:
    source = inp.read()
    
outp = lambda: open(filename + '.S', 'w')
compile(filename, source, outp)
