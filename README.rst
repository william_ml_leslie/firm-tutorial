Tutorial language for the python libfirm wrapper
================================================

:Author: William ML Leslie
:Licence: GNU General Public Licence v3.0 or later

Firm_ is a library that provides a graph-based intermediate
representation, optimizations, and assembly code generation suitable
for use in both AOT and JIT compilers.

.. _Firm: http://pp.ipd.kit.edu/firm/Index

This is an example compiler, written as a tutorial for the python-firm
library.
