==================================
A sample compiler with python-firm
==================================

.. contents::

.. _basics:

Firm Basics
-----------

We're going to use FIRM to turn an Abstract Syntax Tree for a fairly
typical imperative language to assembler output, and see what FIRM
features we can make use of as we go.  Since we're starting from an
AST, we are ignoring the parsing and, to a certain degree, the
type-checking features that you'd find in a real compiler.

In order to compile our functions with FIRM, we will need to build an
IR Graph for each function.  At the highest level, IR Graphs consist
of a number of blocks that represent the control flow within the
function, and each of these blocks contains nodes, representing
computations and data.  The expressions in our source language will
usually be transformed into a number of nodes, and the statements in
our language will often become a number of blocks.

Lets start first with the interfaces we'll require on our AST.

.. _ast:

Designing a simple AST
----------------------

We're going to be making a simple C-like imperitative language, with
both statements and expressions.  First, we will make our AST objects.
These are going to be simple containers that represent the overall
structure of our program, including declarations, types, statements,
and expressions.  We will use the popular attrs_ library.

.. _attrs: https://attrs.readthedocs.io/en/stable/

.. code-block:: python

    import attr

    class Visitable(object):
        is_expression = is_control = False
        def visit(self, visitor, *args, **kwargs):
            m = getattr(visitor, 'visit_' + self._visit_name)
            return m(*(args + attr.astuple(self, recurse=False)), **kwargs)

    ENV = {}

    def register(ast):
        ast = attr.s(ast)
        ENV[ast.__name__] = ast
        return ast

    @register
    class If(Visitable):
        _visit_name = 'if'
        cond = attr.ib()
        true = attr.ib()
        false = attr.ib()

Our ``If`` node has three attributes.  The ``cond`` attribute is a
boolean-valued expression that will decide which branch to run.  The
``true`` and ``false`` attributes hold the statements that will be
selectively branched to.

The only functionality that we have given our ``If`` node is the ``visit``
method it has inherited from Visitable.  ``if_node.visit(visitor, arg)``
will call the method ``visitor.visit_if(arg, cond, true, false)``.  Why
do we want to use a visitor pattern, rather than just adding
``.compile()``, ``.define()`` and ``.typecheck()`` methods to each node?
The main reason is that it makes the tracebacks more readable.  Seeing
a whole page of methods with the same name, especially in the same
module, like so::

  File "firmetude/language.py", line 119, in compile
    res = self.value.compile(env)
  File "firmetude/language.py", line 172, in compile
    left = self.left.compile(env)
  File "firmetude/language.py", line 511, in compile
    block = base.compile(env)
  File "firmetude/language.py", line 119, in compile
    res = self.value.compile(env)

is not as helpful as::

  File "firmetude/compiler.py", line 119, in visit_not
    res = value.visit(env)
  File "firmetude/ast.py", line 7, in visit
    return m(*(args + attr.astuple(self, recurse=False)), **kwargs)
  File "firmetude/compiler.py", line 172, in visit_add
    left = left.visit(env)
  File "firmetude/ast.py", line 7, in visit
    return m(*(args + attr.astuple(self, recurse=False)), **kwargs)
  File "firmetude/compiler.py", line 511, in visit_let
    block = base.visit(env)
  File "firmetude/ast.py", line 7, in visit
    return m(*(args + attr.astuple(self, recurse=False)), **kwargs)
  File "firmetude/compiler.py", line 119, in visit_not
    res = value.visit(env)

It also means we can keep all of the compiler logic together, and all
of the type checking logic, etc.  Lets take a look at some more of our
nodes:

.. code-block:: python

    @register
    class SetLocal(Visitable):
        _visit_name = 'set_local'
        name = attr.ib()
        value = attr.ib()

    @register
    class GetVariable(Visitable):
        is_expression = True
        _visit_name = 'get_variable'
        name = attr.ib()

We use the ``@register`` function we created above to call ``@attr.s`` on
our nodes, and to record them in the ENV dictionary, which is how the
parser will build our nodes.

Here we have both a ``GetVariable`` node, which is an expression (that
is, it produces a value), and a ``SetLocal`` node, which is a statement.
They each have a string name. The ``GetVariable`` node knows nothing
about its scope, so we are going to have to do some work to figure out
if it refers to a local variable, argument, or global variable when we
typecheck and compile it.

.. code-block:: python

    @register
    class Compare(Visitable):
        is_control = True
        _visit_name = 'compare'
        comparison = attr.ib()
        left = attr.ib()
        right = attr.ib()

Here is the ``Compare`` node, to represent comparisons like ``breakfast
== bacon`` and ``temperature > 20``.  Because it always returns a boolean
value, we'll give it the special ``is_control`` classification, which will
make compiling lazy operators like ``&&`` and ``||`` easier.

.. code-block:: python

    @register
    class FunDef(Visitable):
        _visit_name = 'fundef'
        name = attr.ib()
        ty = attr.ib()
        variables = attr.ib()
        body = attr.ib()

Definitions and declarations, which appear at top-level, can be
represented in our AST too.  I've placed the bound variables and their
types within the function definition, too, so we can find them later.

.. code-block:: python

    @register
    class TyFun(Visitable):
        _visit_name = 'function_type'
        arguments = attr.ib()
        result = attr.ib()

    @register
    class TyStruct(Visitable):
        _visit_name = 'struct_type'
        fields = attr.ib()

Similarly, types are easy to represent.  A function type is made up of
a list of arguments - we represent them with ``(name, type)`` pairs -
and a return type; and a struct is made up of a list of fields, also
``(name, type)`` pairs.

.. _compiling_api:

Compiling: statements, expressions, and handling control flow
-------------------------------------------------------------

The language we are compiling has a distinction between statements and
expressions: Expressions return a value, wheras statements do not.
However, sometimes we will find an expression (typically a ``Call``)
where we might expect a statement.  We could have built an AST node
specifically for this, but since we have one more condition we want to
check for, we will write visit methods on our compiler that adapt
between the two.

Firstly, to visit a node in statement context (no value expected):

.. code-block:: python

    def visit(self, block, expr):
        """Visit an AST node in statement context.

        @return the block it continues into.
        """
        if expr.is_expression:
            block, _ = expr.visit(self, block)
        elif expr.is_control:
            target = self.jump_target()
            expr.visit(self, block, target, target)
            block = target.enter()
        else:
            block = expr.visit(self, block)
        return block

Visiting a statement first *requires* a block.  This block represents
control flow that leads to this statement being executed, it is also
where we will place the first instructions of our statement.  After a
statement has been visited, it also *returns* a block, representing
control flow out of the statement.  Whatever is supposed to execute
after the statement is emitted into this block.

We see here also the interface that expressions will provide: not only
do they return the block representing the control flow once they are
done, they also provide something else: a FIRM IR node representing
their value.  We are free to discard this value: if the expression had
no side effect, the function that FIRM produces will even avoid
computing it.

We can also see the interface for comparisons and other logic
operators - they also take two jump targets.  We will look at these a
little later, but we can find the control flow out of a (settled)
target by calling its ``enter()`` method.

You might observe too that we could have made 'the current block' an
attribute of this class.  In this tutorial, we will pass the current
block around for clarity, but feel free to hide it away wherever you
like.

Now lets look at expression context:

.. code-block:: python

    def visit_expression(self, block, expr):
        """Visit an AST node, returning its continuation block and value.

        @param block the block execution starts from.
        @param expr the AST node to compile.
        @return the continuation block and the value.
        """
        if expr.is_expression:
            return expr.visit(self, block)
        if expr.is_control:
            return self.control_to_expression(block, expr)
        raise AST2FirmException("Statement in expression context", expr)

We haven't much to do here.  We raise an exception if we come across a
statement where we expect a value: the parser we will later write
disallows this anyway, but if we accidentally found our way here, we
would want to know about it so we could track down our bug.

The helper method ``control_to_expression`` looks like this:

.. code-block:: python

    def control_to_expression(self, block, control):
        result, true_case, false_case = self.jump_targets(3)
        self.visit_control(block, true_case, false_case, control)
        result.from_block(true_case.enter())
        result.from_block(false_case.enter())
        block = result.enter()
        return block, block.op_Phi([self.constant(1, MODE_IS),
                                    self.constant(0, MODE_IS)])

Here we see our first actual FIRM instruction being created: a Phi
node.  These instructions describe how to get one value from two or
more different code paths.  If the ``result`` block is entered from
``true_case``, we want the value of this expression to be something
representing True.  We've got no boolean type in our language, so we
will use the integer value ``1``.  Similarly for the ``false_case``, we
want our expression to evaluate to ``0``.

How does FIRM match the ``1`` to the ``true_case``?  A Phi node takes a
list of as many nodes as the block containing it has predecessors.  We
set up the predecessors of the ``result`` block using
``result.from_block()``.  So the value we want for the first case needs
to be the first parameter to the Phi.

Normally, we will use firm's own convenience methods and not have to
write Phi nodes ourself, but we didn't know we'd need this variable
beforehand.

.. code-block:: python

    def visit_control(self, block, true_target, false_target, expr):
        """Visit a boolean-typed AST node and jump to the target.

        @param block         the block execution starts from.
        @param true_target   JumpTarget to jump to if the expr evaluates
                             to true.
        @param false_target  JumpTarget to jump to if the expr evaluates
                             to false.
        @param expr          the AST node to compile.
        """
        if expr.is_control:
            expr.visit(self, block, true_target, false_target)
        elif expr.is_expression:
            block, value = expr.visit(self, block)
            zero = self.constant(0, value.get_mode())
            self.visit_compare(block, false_target, true_target,
                               '==', value, zero)
        else:
            raise AST2FirmException("Not valid control", expr)

We treat an expression in control context much like C does.  If an
integer or pointer appears in a conditional, we will compare it with
zero.  If it is equal to zero, we will interpret that as false.

Note that firm IR nodes have a ``get_mode()`` method.  Nodes can
represent control flow, native addresses, a tuple of values, as well
as floating-point and integer values of various precisions.  Many
nodes, such as ``Add`` and ``Cmp``, require that both of their arguments
are the same mode; so we build the constant 0 of the right mode in
``visit_control``.

Compiling: expressions
----------------------

Lets first have a look at some expressions.  Operators like ``+``, ``-``,
``*``, ``/`` and ``%`` require no special handling, so we'll just grab the
operator and apply it to the arguments.  ``op_Div`` and ``op_Mod`` also
have memory effect [0]_ and a control effect if they raise exceptions, but
we will do as C does and ignore that control effect - this makes
division by zero undefined behaviour.

.. [0] python-firm handles memory effects for you by default.  While
       you can pass the ``irn_mem`` keyword argument to most node
       factory functions that accept them, and you can use the ``pn_M``
       attributes of those nodes to get the result memory, you will
       probably also need to prevent the MemoryPolicy from setting the
       default store.  This is currently undocumented.

To fish out the result of a division to return it, we use the pn_res
attribute of the operation.  Operations that return multiple results
have some attributes and methods beginning with ``pn_``, which project
out the individual values.

.. code-block:: python

    SIMPLE_EXPR = {
        '+' : 'op_Add',
        '-' : 'op_Sub',
        '*' : 'op_Mul',
        '/' : 'op_Div',
        '%' : 'op_Mod'
    }

    def visit_simple_exp(self, block, name, *args):
        arg_values = []
        for arg in args:
            block, arg_value = self.visit_expression(block, arg)
            arg_values.append(arg_value)
        if name in ('/', '%'):
            op = getattr(block, SIMPLE_EXPR[name])(*arg_values)
            result = op.pn_res
        else:
            result = getattr(block, SIMPLE_EXPR[name])(*arg_values)
        return block, result

