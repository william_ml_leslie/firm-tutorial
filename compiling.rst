===================
Compiling with FIRM
===================

In order to look at just what makes FIRM tick, and how to represent
functions and types accurately, we will use a compiler for a simple
example language.  This language is somewhat C-like, with statements
and expressions, first-class mutibility, and imperative control flow.
Compiling a functional language is even easier with FIRM, but dealing
with an imperitive language here will let us examine more of FIRM's
special features, and how to write a compiler in their presence.

Reading the compiler
====================

All up, this compiler is about 1000 lines, making it easily readable.
The components that deal directly with FIRM are the modules ``type``,
``firmsupport``, and ``driver``.  We can sum up the modules like so:

firmetude.ast
  Input programs are turned into this uniform representation that is
  easy to walk over and analyse.  This module contains a number of
  classes representing distinct concepts, such as ``If`` statements
  and ``Compare`` expressions.

  Having an ``Abstract Syntax Tree`` makes it easy to write different
  passes over the code.  You will find that this is not a bad place to
  start reading the compiler, as even though there is nothing specific
  to using FIRM here - it's just a bunch of classes for flat data - it
  highlights where to start when adding features to a language such as
  this tutorial language.

firmetude.parser
  The parser is able to read programs written using a human-friendly
  notation and produce a more machine-friendly AST representation.
  This module generates a parser with the Parsley library, but you
  could just as easily use ANTLR, PLY, pyparsing, an existing parser
  for your source language, or even write a parser by hand!  A fun
  experiment might be to fit pycparser to a compiler such as this
  example one, and see how much C you can compile with it.

firmetude.sort
  Some languages require the user to manually sort declarations so
  that usages come after definitions.  I personally find that very
  annoying, so the example compiler includes the code needed to
  toposort declarations.

firmetude.driver
  The entry point to the compiler, the driver co-ordinates the
  different passes.  First the source code is parsed into an AST, then
  the definitions in the AST are sorted, then the AST is typechecked,
  then it is compiled into FIRM IR, then FIRM produces assembler as
  output.

firmetude.type
  This module builds types for FIRM to refer back to, such as struct
  types, array types, pointer types, and primitive types.  It also
  infers and checks types for functions and expressions.  When
  building FIRM IR, the type module is sometimes used to determine the
  type of an expression.

firmetude.firmsupport
  This module is the meat of the compiler, taking each statement or
  expression and producing FIRM IR to represent it.  We are going to
  pick a few methods in particular from this module to highlight how
  to compile code using FIRM.

Minimal driver
--------------

The provided driver has a lot of code to understand from the
beginning, so lets quickly look at a minimal driver, with just what
you need to get started compiling FIRM.

.. code-block:: python

    from firm import types, function
    from firm.base import libfirm
    from firm.types import Method, INT_32, MODE_IS
    from firm.function import Function, IRGraph

    OUTPUT_PREFIX = '/tmp/minimal-driver-example'

    def minimal_driver():
        # Produce the required function type.
        # this example takes three ints, and returns a single int.
        signature = Method.new([INT_32] * 3, [INT_32])

        # Declare the function, giving it a name
        func = Function.new(signature, b"simple")

        # XXX: use 'graph' to describe what the function should do
        graph = IRGraph.new(func, 5)
        block = graph.current_block

        # Build a value for the target machine (a signed integer)
        number_7 = Tarval.from_int(7, MODE_IS)
        # create a constant node representing it
        const_7 = block.op_Const(number_7)
        # return the constant from the function
        result = block.op_Return([const_7])
        # jump to the end block from the return
        graph.end_block.add_predecessor(result)
        # we have finished adding operations to this block
        block.mature()

        # mark this function as complete
        graph.finalise()
        try:
            # check the function for obvious errors
            graph.verify()
        except FirmException:
            # dump this function's graph out for debugging
            with open(OUTPUT_PREFIX + '.vcg', 'w') as dbg_graph:
                graph.dump_graph(dbg_graph)
            raise

        # lower struct and array usage into pointer arithmetic
        libfirm.lower_highlevel()
        # lower into nodes specific to the target CPU
        libfirm.be_lower_for_target()
        # write out the assembler file
        with open(OUTPUT_PREFIX + '.S', 'w') as out:
            libfirm.be_main(out, b'minimal-driver')


You may notice that libfirm did not have to be told which function it
should emit with be_main: FIRM has some global state, which can make it
difficult to test.  It can be useful in practice to have a new process
for each unit of compilation.

Nevertheless, this gives us some idea of the steps that are required
before we go building code: we need an IRGraph to start building code
with, and we need to push the graphs through the firm backend and into
an assembler file.

With that out of the way, we can begin turning code into IRGraphs!  We
will look at nodes that illustrate different features, such as how to
do control flow, how to implement local variables, how to access
arguments, dealing with data structures, et-cetera.  Do feel welcome
however to look at AST nodes in whichever order you like - the
compiler is small enough that you won't get lost.

Lets start by compiling some statments.


Statements
==========

Statements require very little in order to be compiled correctly.
Often a small amount of context information might be required, such as
which variables are bound, what catch handlers are in place, or which
loop a blank ``break`` jumps to.

For our example, we will only pass one bit of context information to
the functions that compile statements: a FIRM block, to which the
statement's operations are to be added.  A statement might do further
control flow, so a function that compiles statements should also
return the block representing normal control-flow out of the
statement.

.. code-block:: python

    continue_block = statement.visit(ast_to_firm, entry_block)

You could use a different mechanism for tracking the current block -
it could be an attribute of the class that does the compiling, for
example - but in this example, it is a parameter and return-value for
the sake of clarity.

We will begin with a statment that demonstrates how the statement
flows from source to FIRM IR.


Begin -- introducing statements
-------------------------------

The ``Begin`` statement is a list of statements that are executed
sequentially from left to right.  Syntactically, it is a list of
semicolon-terminated or semicolon-separated statements.  You might
recognize that function bodies, as well as the bodies of loops and
``If`` statements, are typically of this form.  We represent this in
Parsley with the following code:

.. code-block:: Parsley

    statements = statement:first ws ';' statements:rest -> [first] + rest
               | statement:s -> [s]
               | -> []
    begin = ws '{' statements:s ws '}' -> Begin(s)


Type-checking a statement is easy: A Begin Statement is valid if all
of the statements it contains are valid.  Here is the method in
firmetude/type.py that type-checks the begin statement.

.. code-block:: Python

    def visit_begin(self, block, *body):
        for b in body:
            b.visit(self)

Now to compile a begin statement to FIRM, we need one more thing: the
block at which the statement is to begin executing.  Compiling the
statement will add all the instructions and control-flow necessary to
implement it to the block, and possibly create new blocks in the
process, and then return a block representing control-flow past the
end of the statement and into the next one.

.. code-block:: Python

    def visit_begin(self, block, body):
        for b in body:
            block = b.visit(self, block)
        return block


Return -- introducing expressions
---------------------------------

.. code-block:: Parsley
    ret = ws 'return' expr:value -> Return(value)

A return statement computes a value to return to its caller, and then
immediately jumps to the end of the function.  We should check that
the type of value returned matches the return type of the function:

.. code-block:: python

    def visit_return(self, value):
        assert value.visit(self) == self.return_type

When compiling the return statement, we first need to make sure the
value to be returned has been compiled.  When visiting an expression,
we supply just the block that it should start at, just like compiling
a statement.  However, visting an expression returns not only the
block that represents control-flow out of the expression, but also a
value.

The grammar and AST ensure that we really have an expression here, so
it is safe to unpack the return value.  If that is not the case,
different visitors may need to be used for expressions and statements.

.. code-block:: python

    def visit_return(self, block, value):
        block, value = value.visit(self, block)
        result = block.op_Return([value])
        self.graph.end_block.add_predecessor(result)
        return self.graph.op_Block([block.op_Bad(MODE_X)])

This method has a few unique features: We've created a FIRM node
directly with the op_Return method of blocks.  This produces a
``firm.node.Node`` object, specifically, a ``firm.operations.Return``.

A Node has an associated sort called a mode, which describes just what
the node represents.  It may represent a machine integer, or a
side-effect, or a tuple of values; but a Return node represents
control flow, so it has MODE_X.  Because a return operation should be
followed by the end of the function, we use the add_predecessor method
of the end_block to jump there following a return.

Finally, we return an unreachable block from this function, so that
any code emitted after the return is ignored.


If -- introducing jump targets
------------------------------

Sometimes, boolean values are used for control flow.  In that case,
there is often no need to store them into a register at all.  To that
end, our example compiler represents expressions that are probably
going to be used for control flow slightly differently again.  Notice
the definition for ``visit_if`` in the compiler.

.. code-block:: python

    def visit_if(self, block, condition, true_branch, false_branch):
        after, true_target, false_target = self.jump_targets(3)
        condition.visit(self, block, true_target, false_target)
        after.from_block(true_branch.visit(self, true_target.enter()))
        after.from_block(false_branch.visit(self, false_target.enter()))
        return after.enter()

Visiting the condition doesn't just take a visitor and a block - it
also takes two parameters, here true_target and false_target.  These
indicate where the condition should jump to if the condition turns out
to be true or false respectively.  It also returns nothing at all.

What are these targets?  The ``JumpTarget`` class is defined within
the ``firmsupport`` module.  It serves to prevent spurious blocks from
being built - if one block unconditionally jumps to a target, and that
target has no other predecessors, it is better to emit code directly
into the original block.

Two methods of the jump target are used here: The ``from_block``
method sets up an unconditional jump from its parameter.  After the
``Begin`` statement representing the true branch has been executed,
for example, it should jump to after the ``If`` statement.  The
``enter`` method finalises the jump target, returning the associated
block.  It is here that the "spurious block" condition is checked.


While - introducing enter_immature
----------------------------------

Note that when compiling ``While`` we need to make sure that the body
jumps back up to the start of the condition checking, not just back to
the body.  This means that the condition check really needs its own
block.

This requires a slightly different use of ``JumpTarget``, because we
need to add code to the condition block before we've built all of its
predecessors.  We can use the ``enter_immature`` method to create a
concrete block ahead of time.  We can mature the block once we've
added all of its predecessors.

.. code-block:: python

    def visit_while(self, block, condition, body):
        tcond, tbody, tafter = self.jump_targets(3)
        tcond.from_block(block)
        condition.visit(self, tcond.enter_immature(), tbody, tafter)
        tcond.from_block(body.visit(self, tbody.enter()))
        tcond.block.mature()
        return tafter.enter()


SetLocal - introducing local variables
--------------------------------------

One challenge of working with SSA form, where each node uses previous
nodes to build its values, is that mutable local variables take some
work to organise.  Doing it by hand requires building Phi nodes
wherever two different values may arise.  Thankfully, FIRM can take
care of these for you, and present a much more natural imperative API.

A graph's local variables need to be allocated ahead of time.  The
``build_graph()`` method of the Function object can accept the
``n_locals`` keyword argument for this purpose.  Once these locals are
allocated, you only need to know the index of the local you want to
access, and the mode of the value when reading.

.. code-block:: python

    def visit_set_local(self, block, name, value):
        block, value = value.visit(self, block)
        block.set(self.local_numbers[name] - self.nargs, value)
        return block


SetIndex - introducing pointer arithmetic
-----------------------------------------

Write effects happen on single pointers.  That pointer can be
calculated with the Sel node for arrays, and the Member node for
structs.  Note that stores also take a type: one optional optimisation
uses this to detect aliasing.

.. code-block:: python

    def visit_set_index(self, block, target0, index, value):
        array_type = target0.visit(self.ast_typer).target
        block, target = target0.visit(self, block)
        block, index = index.visit(self, block)
        block, value = value.visit(self, block)
        sel = block.op_Sel(target, index, array_type)
        op = block.op_Store(sel, value, array_type.element_type, 0)
        return block


Expressions
===========

Expressions are very easy to compile with FIRM.  Compiling an
expression with our compiler requires a block to start working from,
and returns both a block to continue from and a value, in the form of
a ``firm.node.Node``.  Lets take a look at a few expressions.

.. code-block:: python

    continue_block, value = expression.visit(ast_to_firm, entry_block)

SimpleExp
---------

Simple expressions, including ``a + b``, ``x * y`` and ``n % m``
require little more than a call to the right op_ method.  For example,
addition requires simply

.. code-block:: python

    block, a_node = a.visit(self, block)
    block, b_node = b.visit(self, block)
    return block, block.op_Add(a_node, b_node)

Division and modulo are slightly more involved, as they possibly can
involve control flow: some processors trap on division by zero, for
example.  These operators return a tuple, including a memory value
which python-firm handles by default, and the result value, which we
need to fish out.

.. code-block:: python

    block, n_node = n.visit(self, block)
    block, m_node = m.visit(self, block)
    return block, block.op_Mod(n_node, m_node).pn_res

Our example compiler implements this in a very general way, in case we
want to add more simple operators later.

.. code-block:: python

    def visit_simple_exp(self, block, name, *args):
        arg_values = []
        for arg in args:
            block, arg_value = arg.visit(self, block)
            arg_values.append(arg_value)
        if name in ('/', '%'):
            op = getattr(block, SIMPLE_EXPR[name])(*arg_values)
            result = op.pn_res
        else:
            result = getattr(block, SIMPLE_EXPR[name])(*arg_values)
        return block, result
