import datetime
from firmetude.parser import GRAMMAR
from firmetude.sort import toposort, SortDecls
from firmetude.type import DeclTyper, AstTyper
from firmetude.firmsupport import ASTToFirm
from firmetude.ast import FunDef, MALLOC
from firm.base import libfirm, FirmException

def compile(name, source, output):
    # 0. parse the source into an abstract syntax tree.
    print datetime.datetime.now(), 'parsing...'
    top_level = GRAMMAR(source).toplevel()

    print datetime.datetime.now(), 'sorting types...'
    # 1. sort the types so that each one is declared before we need to
    # use them.
    sd = SortDecls()
    by_name = {}
    for definition in top_level:
        definition.visit(sd)
        by_name[definition.name] = definition
    sorted_names = toposort(sd.deps)
    sorted_defs = [by_name[name] for name in sorted_names]

    print datetime.datetime.now(), 'building types...'
    # 2. create the types and objects to represent the functions.
    dt = DeclTyper()
    MALLOC.visit(dt)
    for definition in sorted_defs:
        definition.visit(dt)
    while dt.later:
        later = dt.later
        dt.later = []
        for callback in later:
            callback()

    print datetime.datetime.now(), 'checking types...'
    # 3. check that the expressions in each of the functions have the
    # correct type.
    for definition in sorted_defs:
        if isinstance(definition, FunDef):
            names, types = dt.local_types[definition.name]
            astt = AstTyper(definition.name, names, types, dt)
            definition.body.visit(astt)

    print datetime.datetime.now(), 'building IR'
    # 4. Build FIRM IR from each of the functions.
    for definition in sorted_defs:
        if isinstance(definition, FunDef):
            names, types = dt.local_types[definition.name]
            astt = AstTyper(definition.name, names, types, dt)
            nargs = len(dt.type_env[definition.name].params)
            graph = dt.global_env[definition.name].build_graph(
                n_locals=len(names) - nargs)
            builder = ASTToFirm(graph, names, types, nargs, astt,
                                dt.global_env)
            block = definition.body.visit(builder, graph.current_block)
            if definition.ty.result is None:
                graph.end_block.add_predecessor(block.op_Return([]))
            assert len(graph.end_block.cfgpred) > 0
            graph.finalise()
            try:
                graph.verify()
            except FirmException:
                with open(definition.name + '.vcg', 'w') as deb:
                    graph.dump_graph(deb)
                raise

    print datetime.datetime.now(), 'compiling...'
    libfirm.lower_highlevel()
    libfirm.be_lower_for_target()
    with output() as f:
        libfirm.be_main(f, name)
    print datetime.datetime.now(), 'done.'

