import attr

class Visitable(object):
    is_expression = is_control = False
    def visit(self, visitor, *args, **kwargs):
        m = getattr(visitor, 'visit_' + self._visit_name)
        return m(*(args + attr.astuple(self, recurse=False)), **kwargs)

ENV = {}

def register(ast):
    ast = attr.s(ast)
    ENV[ast.__name__] = ast
    return ast

@register
class If(Visitable):
    _visit_name = 'if'
    cond = attr.ib()
    true = attr.ib()
    false = attr.ib()

@register
class While(Visitable):
    _visit_name = 'while'
    cond = attr.ib()
    body = attr.ib()

@register
class Begin(Visitable):
    """A begin node (a list of statements).

    @attr body a list of nodes
    """
    _visit_name = 'begin'
    body = attr.ib()

@register
class SetLocal(Visitable):
    _visit_name = 'set_local'
    name = attr.ib()
    value = attr.ib()

@register
class GetVariable(Visitable):
    is_expression = True
    _visit_name = 'get_variable'
    name = attr.ib()

@register
class GetArgument(Visitable):
    is_expression = True
    _visit_name = 'get_argument'
    name = attr.ib()

@register
class SimpleExp(Visitable):
    is_expression = True
    _visit_name = 'simple_exp'
    operation = attr.ib()
    left = attr.ib()
    right = attr.ib()

@register
class Return(Visitable):
    _visit_name = 'return'
    value = attr.ib()

@register
class Call(Visitable):
    is_expression = True
    _visit_name = 'call'
    fn = attr.ib()
    args = attr.ib()
    #ty = attr.ib(init=False, default=None)

@register
class GetIndex(Visitable):
    is_expression = True
    _visit_name = 'get_index'
    base = attr.ib()
    index = attr.ib()

@register
class SetIndex(Visitable):
    _visit_name = 'set_index'
    base = attr.ib()
    index = attr.ib()
    value = attr.ib()

@register
class GetField(Visitable):
    is_expression = True
    _visit_name = 'get_field'
    base = attr.ib()
    field = attr.ib()

@register
class SetField(Visitable):
    _visit_name = 'set_field'
    base = attr.ib()
    field = attr.ib()
    value = attr.ib()

@register
class Compare(Visitable):
    is_control = True
    _visit_name = 'compare'
    comparison = attr.ib()
    left = attr.ib()
    right = attr.ib()

@register
class Constant(Visitable):
    is_expression = True
    _visit_name = 'constant'
    value = attr.ib()
    ty = attr.ib()

@register
class NewStruct(Visitable):
    is_expression = True
    _visit_name = 'new_struct'
    ty = attr.ib()

@register
class NewArray(Visitable):
    is_expression = True
    _visit_name = 'new_array'
    ty = attr.ib()
    size = attr.ib()

@register
class ExprAsStatement(Visitable):
    _visit_name = 'expr_as_statement'
    value = attr.ib()

@register
class CondAsStatement(Visitable):
    _visit_name = 'cond_as_statement'
    value = attr.ib()

@register
class CondAsExpr(Visitable):
    _visit_name = 'cond_as_expr'
    value = attr.ib()

@register
class ExprAsCond(Visitable):
    _visit_name = 'expr_as_cond'
    value = attr.ib()

@register
class ReifyBool(Visitable):
    is_expression = True
    _visit_name = 'bool'
    value = attr.ib()

@register
class FunDef(Visitable):
    _visit_name = 'fundef'
    name = attr.ib()
    ty = attr.ib()
    variables = attr.ib()
    body = attr.ib()

@register
class FunDecl(Visitable):
    _visit_name = 'fundecl'
    name = attr.ib()
    ty = attr.ib()

@register
class TyDef(Visitable):
    _visit_name = 'typedef'
    name = attr.ib()
    value = attr.ib()

@register
class TyFun(Visitable):
    """Function type (a : A, b : B) -> R

    @attr arguments a list of (name, Ty) pairs.
    @attr result Type of the function's results.
    """
    _visit_name = 'function_type'
    arguments = attr.ib()
    result = attr.ib()

@register
class TyStruct(Visitable):
    """Struct type {x : X, y : Y}

    @attr fields a list of (name, Ty) pairs.
    """
    _visit_name = 'struct_type'
    fields = attr.ib()

@register
class TyStructO(Visitable):
    """Struct type, explicit offsets {x @ 0 : X, y @ 4 : Y}

    @attr fields a list of (name, Ty, offset) tuples.
    """
    _visit_name = 'struct_typeo'
    fields = attr.ib()


@register
class TyArray(Visitable):
    _visit_name = 'array_type'
    element_type = attr.ib()


@register
class TyPointer(Visitable):
    _visit_name = 'pointer_type'
    element_type = attr.ib()


MALLOC = FunDecl('malloc', TyFun([('size', GetVariable('uint'))],
                                 TyPointer(GetVariable('void'))))
