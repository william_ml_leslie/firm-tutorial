from firm.types import INT_32, UINT_32, MODE_X
from firm.tarval import Tarval
from firm.bitfields import Relation
from firmetude.ast import Constant, SimpleExp, GetVariable


COMPARATORS = {
    '==' : Relation.EQUAL,
    '!=' : Relation.LESS_GREATER,
    '<' : Relation.LESS,
    '>' : Relation.GREATER,
    '<=' : Relation.LESS_EQUAL,
    '>=' : Relation.GREATER_EQUAL
}

SIMPLE_EXPR = {
    '+' : 'op_Add',
    '-' : 'op_Sub',
    '*' : 'op_Mul',
    '/' : 'op_Div',
    '%' : 'op_Mod'
}

MODE = {
    'int' : INT_32.mode,
    'uint' : UINT_32.mode
}


class ASTToFirm(object):
    def __init__(self, graph, localnames, localtypes, nargs, ast_typer,
                 global_env):
        self.graph = graph
        self.localtypes = localtypes
        self.local_numbers = dict((n, i) for i, n in enumerate(localnames))
        self.nargs = nargs
        self.ast_typer = ast_typer
        self.global_env = global_env

    def jump_target(self):
        return JumpTarget(self.graph)

    def jump_targets(self, n):
        return [JumpTarget(self.graph) for _ in range(n)]

    def constant(self, value, mode):
        tarvalue = Tarval.from_int(value, mode)
        return self.graph.op_Const(tarvalue._ir_val)

    def visit_cond_as_expr(self, block, control):
        result, true_case, false_case = self.jump_targets(3)
        control.visit(self, block, true_case, false_case)
        result.from_block(true_case.enter())
        result.from_block(false_case.enter())
        block = result.enter()
        return block, block.op_Phi([self.constant(1, MODE_IS),
                                    self.constant(0, MODE_IS)])

    def visit_expr_as_statement(self, block, expr):
        block, _ = expr.visit(self, block)
        return block

    def visit_cond_as_statement(self, block, expr):
        result = self.jump_target()
        control.visit(self, block, result, result)
        return result.enter()

    def visit_if(self, block, condition, true_branch, false_branch):
        after, true_target, false_target = self.jump_targets(3)
        condition.visit(self, block, true_target, false_target)
        after.from_block(true_branch.visit(self, true_target.enter()))
        after.from_block(false_branch.visit(self, false_target.enter()))
        return after.enter()

    def visit_while(self, block, condition, body):
        tcond, tbody, tafter = self.jump_targets(3)
        tcond.from_block(block)
        condition.visit(self, tcond.enter_immature(), tbody, tafter)
        tcond.from_block(body.visit(self, tbody.enter()))
        tcond.block.mature()
        return tafter.enter()

    def visit_begin(self, block, body):
        for b in body:
            block = b.visit(self, block)
        return block

    def visit_return(self, block, value):
        block, value = value.visit(self, block)
        result = block.op_Return([value])
        self.graph.end_block.add_predecessor(result)
        return self.graph.op_Block([block.op_Bad(MODE_X)])

    def visit_call(self, block, fn, args):
        fun_type = fn.visit(self.ast_typer)
        block, target = fn.visit(self, block)
        argument_values = []
        for arg in args:
            block, value = arg.visit(self, block)
            argument_values.append(value)
        call = block.op_Call(target, argument_values, fun_type)
        if len(fun_type.results) == 0:
            return block, self.graph.op_Bad(INT_32.mode)
        return block, block.op_Proj(call.pn_T_result,
                                    fun_type.results[0].mode, 0)

    def visit_simple_exp(self, block, name, *args):
        arg_values = []
        for arg in args:
            block, arg_value = arg.visit(self, block)
            arg_values.append(arg_value)
        if name in ('/', '%'):
            op = getattr(block, SIMPLE_EXPR[name])(*arg_values)
            result = op.pn_res
        else:
            result = getattr(block, SIMPLE_EXPR[name])(*arg_values)
        return block, result

    def visit_get_field(self, block, target, field):
        target_ty = target.visit(self.ast_typer).target
        field_ent = target_ty.get_named(field)
        block, target = target.visit(self, block)
        interior_ptr = block.op_Member(target, field_ent)
        op = block.op_Load(interior_ptr, field_ent.type.mode,
                           target_ty, 0)
        return block, op.pn_res

    def visit_get_index(self, block, target, index):
        array_type = target.visit(self.ast_typer).target
        block, target = target.visit(self, block)
        block, index = index.visit(self, block)
        mode = array_type.element_type.mode
        sel = block.op_Sel(target, index, array_type)
        op = block.op_Load(sel, mode, array_type.element_type, 0)
        return block, op.pn_res

    def visit_get_variable(self, block, name):
        try:
            number = self.local_numbers[name]
        except KeyError:
            return self.get_global(block, name)
        if number < self.nargs:
            return block, block.op_Proj(self.graph.start.pn_T_args,
                                        self.localtypes[number].mode,
                                        number)
        var = block.get(number - self.nargs, self.localtypes[number].mode)
        return block, var

    def get_global(self, block, name):
        return block, self.graph.op_Address(self.global_env[name])

    def visit_set_field(self, block, target, field_name, value):
        struct_type = target.visit(self.ast_typer).target
        field = struct_type.get_named(field_name)
        block, target = target.visit(self, block)
        block, value = value.visit(self, block)
        interior_ptr = block.op_Member(target, field)
        op = block.op_Store(interior_ptr, value, field.type, 0)
        return block

    def visit_set_index(self, block, target_, index, value):
        array_type = target_.visit(self.ast_typer).target
        block, target = target_.visit(self, block)
        block, index = index.visit(self, block)
        block, value = value.visit(self, block)
        sel = block.op_Sel(target, index, array_type)
        op = block.op_Store(sel, value, array_type.element_type, 0)
        return block

    def visit_set_local(self, block, name, value):
        block, value = value.visit(self, block)
        block.set(self.local_numbers[name] - self.nargs, value)
        return block

    def visit_constant(self, block, value, mode):
        return block, self.constant(value, MODE.get(mode, mode))

    # Control

    def visit_expr_as_cond(self, block, true_target, false_target, expr):
        zero = Constant(0, expr.visit(self.ast_typer).mode)
        self.visit_compare(block, false_target, true_target, '==', expr, zero)

    def visit_not(self, block, true_target, false_target, value):
        expr.value.visit(self, block, false_target, true_target)

    def visit_and(self, block, true_target, false_target, left, right):
        extra_target = self.jump_target()
        left.visit(self, block, extra_target, false_target, left)
        right.visit(self, extra_target.enter(),
                    true_target, false_target, right)

    def visit_or(self, block, true_target, false_target, left, right):
        extra_target = self.jump_target()
        left.visit(self, block, extra_target, false_target, left)
        right.visit(self, extra_target.enter(),
                    true_target, false_target, right)

    def visit_compare(self, block, true_target, false_target,
                      relation, left,right):
        block, left = left.visit(self, block)
        block, right = right.visit(self, block)
        op_cmp = block.op_Cmp(left, right, COMPARATORS[relation])
        op_cond = block.op_Cond(op_cmp)
        true_target.from_predicate(op_cond.pn_true)
        false_target.from_predicate(op_cond.pn_false)

    def visit_new_array(self, block, ty, size):
        array_type = self.ast_typer.visit_new_array(ty, size).target
        elem_size = array_type.element_type.size
        return self.malloc(block,
                           SimpleExp('*', Constant(elem_size, UINT_32.mode),
                                     size))

    def visit_new_struct(self, block, ty):
        struct_size = self.ast_typer.visit_new_struct(ty).target.size
        return self.malloc(block, Constant(struct_size, UINT_32.mode))

    def malloc(self, block, size):
        return self.visit_call(block, GetVariable(b'malloc'), [size])



class JumpTarget(object):
    def __init__(self, graph):
        self.graph = graph
        self.block = None
        self.first = False

    def get_target(self, absolute=False):
        block = self.block
        if block is None or self.first:
            self.block = self.graph.immature_block()
            if self.first:
                self.block.add_predecessor(block.op_Jmp())
            self.first = absolute and not self.first
        return self.block

    def from_block(self, block):
        if self.block is None:
            self.block = block
            self.first = True
            return
        self.get_target(absolute=True).add_predecessor(block.op_Jmp())
            
    def from_predicate(self, control):
        self.get_target().add_predecessor(control)

    def enter_immature(self):
        return self.get_target()

    def enter(self):
        block = self.block
        if block is None:
            raise Exception('entering a block with no predecessors')
        if not block.matured:
            block.mature()
        return block

    def __repr__(self):
        return 'jump_target(%s, %s)' % (self.block, self.first)


class AST2FirmException(Exception):
    pass
