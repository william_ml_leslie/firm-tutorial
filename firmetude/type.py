from firm.types import (Primitive, Pointer, Array, INT_32, UINT_32, Method,
                        Struct, VOID)
from firm.function import Function
from firm.bitfields import TypeState
from itertools import count


class DeclTyper(object):
    def __init__(self):
        self.later = []
        self.type_env = {
            'int' : INT_32,
            'uint' : UINT_32,
            'void' : VOID
        }
        self.local_types = {}
        self.global_env = {}

    # three types of declarations will all add something to type_env.
    # function definitions will also add the function object to the
    # global environment.

    def visit_fundef(self, name, ty, variables, body):
        method_type  = ty.visit(self, name)
        self.type_env[name] = method_type
        self.global_env[name] = Function.new(method_type, name)
        local_names = [n for n, _ in (ty.arguments + variables)]
        local_types = []
        local_types.extend(method_type.params)
        local_types.extend(var.visit(self, "%s.%s" % (name, varname))
                           for varname, var in variables)
        self.local_types[name] = local_names, local_types

    def visit_typedef(self, name, value):
        self.type_env[name] = value.visit(self, name)

    def visit_fundecl(self, name, ty):
        method_type = self.type_env[name] = ty.visit(self, name)
        self.global_env[name] = Function.new(method_type, name)

    # produce firm types for each possible type.

    def visit_function_type(self, name, arguments, result):
        arg_types = [arg.visit(self, '%s.%s' % (name, argname))
                     for argname, arg in arguments]
        if result is None:
            return Method.new(arg_types, [])
        return Method.new(arg_types, [result.visit(self, '%s.$res')])

    def visit_struct_type(self, name, fields):
        struct = Struct.new(name)
        for fieldname, ty in fields:
            fty = ty.visit(self, "%s.%s" % (name, fieldname))
            assert fty is not None, (
                "%s field could not be defined before struct %s" %
                (fieldname, name))
            struct.add_field(fieldname, fty)
        struct.layout_default()
        return struct
    
    def visit_struct_typeo(self, name, fields):
        struct = Struct.new(name)
        size = 0
        alignment = 8
        for fieldname, ty, offset in fields:
            fty = ty.visit(self, "%s.%s" % (name, fieldname))
            assert fty is not None, (
                "%s field could not be defined before struct %s" %
                (fieldname, name))
            struct.add_field(fieldname, fty, offset, fty.alignment)
            size = max(size, offset + fty.size)
            alignment = max(alignment, fty.alignment)
        struct.alignment = alignment
        struct.size = size
        struct.state = TypeState.FIXED
        return struct

    def visit_array_type(self, name, element_type):
        element_ty = element_type.visit(self, name)
        if element_ty is None:
            return None
        return Array.new(element_ty)

    def visit_pointer_type(self, name, target_type):
        target_type0 = target_type.visit(self, name)
        if target_type0 is None:
            result = Pointer.new(None)
            @self.later.append
            def lazy_target():
                result.target = target_type.visit(self, name)
            return result
        return Pointer.new(target_type0)

    def visit_get_variable(self, parent_name, name):
        return self.type_env.get(name)


class AstTyper(object):
    def __init__(self, fname, localnames, localtypes, decl_typer):
        self.fname = fname
        self.decl_typer = decl_typer
        self.global_env = decl_typer.global_env
        self.local_types = dict((n, localtypes[i])
                                for i, n in enumerate(localnames))
        self.local_type_names = {}

    def visit_if(self, condition, true_branch, false_branch):
        assert condition.visit(self) == bool
        true_branch.visit(self)
        false_branch.visit(self)

    def visit_while(self, block, condition, body):
        assert condition.visit(self) == bool
        body.visit(self)

    def visit_begin(self, block, *body):
        for b in body:
            b.visit(self)

    def visit_return(self, value):
        assert value.visit(self) == self.return_type

    def visit_expr_as_statement(self, value):
        value.visit(self)

    def visit_cond_as_statement(self, value):
        value.visit(self)

    def visit_cond_as_expr(self, value):
        assert value.visit(self) == bool
        return UINT_32

    def visit_expr_as_cond(self, value):
        assert value.visit(self) in (INT_32, UINT_32)
        return bool

    def visit_call(self, fn, *args):
        fn_type = fn.visit(self)
        args_types = [arg.visit(self) for arg in args]
        assert isinstance(fn_type, Method)
        assert len(fn_type.params) == len(args_types)
        for param, arg in zip(fn_type.params, args_types):
            assert param == arg
        if len(fn_type.results) == 1:
            return fn_type.results[0]

    def visit_simple_exp(self, name, *args):
        args_types = [arg.visit(self) for arg in args]
        for arg in args_types:
            assert args_types[0] == arg
        return args_types[0]

    def visit_get_field(self, target, field):
        assert isinstance(field, bytes)
        pointer_type = target.visit(self)
        assert isinstance(pointer_type, Pointer)
        struct_type = pointer_type.target
        assert isinstance(struct_type, Struct)
        for subfield in struct_type:
            if subfield.ident == field:
                return subfield.type
        assert False, "missing field " + field

    def visit_get_index(self, target, index):
        pointer_type = target.visit(self)
        assert isinstance(pointer_type, Pointer)
        array_type = pointer_type.target
        assert isinstance(array_type, Array)
        index_type = index.visit(self)
        assert index_type in (INT_32, UINT_32)
        return array_type.element_type

    def visit_get_variable(self, name):
        try:
            return self.local_types[name]
        except KeyError:
            return self.global_env[name].get_type()

    def visit_set_field(self, target, field, value):
        assert isinstance(field, bytes)
        pointer_type = target.visit(self)
        assert isinstance(pointer_type, Pointer)
        struct_type = pointer_type.target
        assert isinstance(struct_type, Struct)

        value_type = value.visit(self)
        for subfield in struct_type:
            if subfield.ident == field:
                assert subfield.type == value_type
                return

        assert False, "missing field " + field

    def visit_set_index(self, target, index, value):
        pointer_type = target.visit(self)
        assert isinstance(pointer_type, Pointer)
        array_type = pointer_type.target
        assert isinstance(array_type, Array)
        index_type = index.visit(self)
        value_type = value.visit(self)
        assert index_type in (INT_32, UINT_32)
        assert array_type.element_type == value_type

    def visit_set_local(self, name, value):
        assert value.visit(self) == self.local_types[name]

    def visit_constant(self, block, value, mode):
        return Primitive.FOR_MODE[mode]

    def visit_not(self, value):
        assert value.visit(self) == bool
        return bool

    def visit_and(self, left, right):
        assert left.visit(self) == bool
        assert right.visit(self) == bool
        return bool

    def visit_or(self, left, right):
        assert left.visit(self) == bool
        assert right.visit(self) == bool
        return bool

    def visit_compare(self, relation, left, right):
        assert left.visit(self) in (INT_32, UINT_32)
        assert right.visit(self) in (INT_32, UINT_32)
        return bool

    def visit_new_struct(self, name):
        struct = self.decl_typer.type_env[name]
        assert isinstance(struct, Struct)
        return Pointer.new(struct)

    def visit_new_array(self, ty, size):
        assert size.visit(self) == UINT_32
        array = self.visit_type(ty)
        assert isinstance(array, Array)
        return Pointer.new(array)

    def visit_type(self, ty):
        try:
            return self.local_types[ty]
        except KeyError:
            firm_ty = ty.visit(self.decl_typer,
                               b'$_%s' % (len(self.local_types),))
            self.local_types[ty] = firm_ty
            return firm_ty
