from firmetude.ast import ENV

import parsley
import datetime

print  datetime.datetime.now(), 'defining grammar...'
GRAMMAR = parsley.makeGrammar("""
  statements = statement:first ws ';' statements:rest -> [first] + rest
             | statement:s -> [s]
             | -> []
  begin = ws '{' statements:s ws '}' -> Begin(s)
  iff   = ws 'if' ws '(' cond:condi ws ')' begin:true ws 'else' begin:false
        -> If(condi, true, false)
        | ws 'if' ws '(' cond:condition ws ')' begin:true
        -> If(condition, true, Begin([]))
  while = ws 'while' ws '(' cond:condition ws ')' begin:body
        -> While(condition, body)
  ret = ws 'return' expr:value -> Return(value)

  setvar = name:n ws '=' expr:value -> SetLocal(n, value)
  setfield = expr_simple:struct ws '.' name:n ws '=' expr:value
           -> SetField(struct, n, value)
           | expr:struct ws '.' name:n ws '=' expr:value
           -> SetField(struct, n, value)
  setindex = expr_getfield:struct ws '[' expr:n ws ']' ws '=' expr:value
           -> SetIndex(struct, n, value)

  op_cmp = '==' | '!=' | '<=' | '>=' | '<' | '>'
  arg = expr:e ws ',' -> e
      | expr

  expr_simple   = name:n -> GetVariable(n)
                | '(' expr:e ws ')' -> e
                | <digit+>:d 'u' -> Constant(int(d), 'uint')
                | <'-'? digit+>:d -> Constant(int(d), 'int')
  expr_call     = expr_simple:f ws '(' arg*:xs ws ')' -> Call(f, xs)
                | expr_simple
  expr_new      = 'new' ws 'struct' ws '(' name:t ws ')' -> NewStruct(t)
                | 'new' ws 'array' ws '(' expr:sz ws ',' ty:t ws ')'
                -> NewArray(t, sz)
                | expr_call
  expr_getfield = expr_new:l ws '.' name:n -> GetField(l, n)
                | expr_new
  expr_getindex = expr_getfield:l ws '[' expr:r ws ']' -> GetIndex(l, r)
                | expr_getfield

  expr_div  = expr_getindex:l ws '/' expr:r -> SimpleExp('/', l, r)
            | expr_getindex:l ws '%' expr:r -> SimpleExp('%', l, r)
            | expr_getindex
  expr_mul  = expr_div:l ws '*' expr:r -> SimpleExp('*', l, r)
            | expr_div
  expr_sub  = expr_mul:l ws '-' expr:r -> SimpleExp('-', l, r)
            | expr_mul
  expr_add  = expr_sub:l ws '+' expr:r -> SimpleExp('+', l, r)
            | expr_sub

  expr      = cond_only:c -> CondAsExpr(c)
            | ws expr_add

  cond_cmp  = expr_add:l ws op_cmp:op expr:r -> Compare(op, l, r)
  cond_or   = cond_cmp:l ws '||' cond:r -> Or(l, r)
            | cond_cmp
  cond_only = ws cond_or:l ws '&&' cond:r -> And(l, r)
            | ws cond_or
  cond      = cond_only
            | ws expr_add:e -> ExprAsCond(e)

  statement = iff | while | ret | setvar | setindex | setfield
            | cond_only:c -> CondAsStatement(c)
            | ws expr_add:e -> ExprAsStatement(e)

  ty = point_ty | struct_ty | function_ty | array_ty | name_ty
     | ('int' | 'uint'):x -> TyPrimitive(x)

  argty = name:n ':' ty:t -> (n, t)
  argtys = argty:x ws ',' argtys:xs -> [x] + xs
         | argty:x                  -> [x]
         |                          -> []
  sfieldty = name:n '@' ws <digit+>:ofs ws ':' ty:t -> (n, t, int(ofs))
  sfieldtys = sfieldty:x ws ',' sfieldtys:xs -> [x] + xs
            | sfieldty:x -> [x]

  ty_or_void  = ws 'Void' -> None
              | ty
  sallocable  = struct_ty | name_ty
  callocable  = ws '[' ws <digit+>
  name_ty     = name:n -> GetVariable(n)
  array_ty    = ws '[' ws ']' ty:el -> TyArray(el)
  struct_ty   = ws 'struct' ws '{' argtys:xs '}'            -> TyStruct(xs)
              | ws 'struct' ws '{' sfieldtys:xs '}'         -> TyStructO(xs)
  function_ty = ws '(' argtys:xs ')' ws '->' ty_or_void:res -> TyFun(xs, res)
  point_ty    = ws '*' ty:el -> TyPointer(el)

  localspec = ws '<' argtys:xs '>' -> xs
            | -> []

  tydef = ws 'typedef' name:n ws '=' ty:t -> TyDef(n, t)
  fundef = ws 'define' name:n function_ty:ty localspec:ls ws begin:body
         -> FunDef(n, ty, ls, body)
  fundecl = ws 'extern' name:n function_ty:ty -> FunDecl(n, ty)

  name = ws <letter letterOrDigit*>:n ws -> n

  toplevel = (fundef | tydef | fundecl)*:x ws -> x
""", ENV)

print datetime.datetime.now(), 'done.'
