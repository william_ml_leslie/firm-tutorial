def toposort(graph):
    result = []
    seen = set()
    while graph:
        stack = [graph.popitem()]
        while stack:
            node, deps = stack[-1]
            if not deps:
                result.append(node)
                seen.add(node)
                stack.pop()
            else:
                dep = deps.pop()
                if dep in stack:
                    raise Exception("Cycle detected",
                                    stack[stack.index(dep):])
                if dep not in seen:
                    seen.add(dep)
                    stack.append((dep, graph[dep]))
    return result


class SortDecls(object):
    def __init__(self):
        self.deps = {}

    def visit_fundef(self, name, ty, variables, body):
        self.deps[name] = ty.visit(self)

    def visit_typedef(self, name, value):
        self.deps[name] = value.visit(self)

    def visit_function_type(self, arguments, result):
        deps = []
        for _, ty in arguments:
            deps.extend(ty.visit(self))
        if result is not None:
            deps.extend(result.visit(self))
        return deps

    def visit_struct_type(self, fields):
        # struct types don't need to be fleshed out before use in a
        # pointer, but do before use in an array.
        return []

    visit_struct_typeo = visit_struct_type

    def visit_array_type(self, element_type):
        return element_type.visit(self)

    def visit_pointer_type(self, element_type):
        return []

    def visit_get_variable(self, name):
        if name in ('int', 'uint', 'Void'):
            return []
        return [name]
